#!/usr/bin/python3

import os
import shutil
import sys 

def prep():
    sys.stdout.write('Preparing connl... ')
    sys.stdout.flush()
        
    working = '../working/'
    conll_src = '../test/conll2014/data/conll14st-preprocessed.m2'
    conll_dst = working + 'conll/'
    
    if not os.path.exists (working):
        os.mkdir(working)
    
    if os.path.exists (conll_dst):
        shutil.rmtree(conll_dst)
    os.mkdir(conll_dst)
    
    sentId = 1
    orig = ''
    cor = ''
    for line in (open(conll_src)):
        if len(line) < 2:
            sentFolder = conll_dst + str(sentId) + '/'
            os.mkdir(sentFolder)
            open(sentFolder + 'orig', 'w').write(orig)
            open(sentFolder + 'cor.m2', 'w').write(cor)
            orig = ''
            cor = ''
            sentId +=1 
        else:
            if line[0] is 'S':
                orig = line[2:].replace(' ', '\n')
            elif line[0] is 'A':
                cor += line[2:]
    
    sys.stdout.write('Done\n')
    sys.stdout.flush()

