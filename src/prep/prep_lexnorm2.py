#!/usr/bin/python3

import json
import os
import shutil
import sys

def prep():
    sys.stdout.write('Preparing lexnorm2015... ')
    sys.stdout.flush()
        
    working = '../working/'
    lexnorm_src = '../test/lexnorm2015/train_data.json'
    lexnorm_dst = working + 'lexnorm2015/'
    
    
    if not os.path.exists(working):
        os.mkdir(working)
    
    if os.path.exists(lexnorm_dst):
        shutil.rmtree(lexnorm_dst)
    os.mkdir(lexnorm_dst)
    
    data = json.load(open(lexnorm_src, 'r'))
    
    for sent in data:
        sentFolder = lexnorm_dst + sent['index'] + '/'
        os.mkdir(sentFolder)
    
        origFile = open(sentFolder + 'orig', 'w')
        for word in sent['input']:
            origFile.write(word + '\n')
        origFile.close()
        
        corFile = open(sentFolder + 'cor', 'w')
        for word in sent['output']:
            corFile.write(word + '\n')
        corFile.close()

    sys.stdout.write('Done\n')
    sys.stdout.flush()

