#!/usr/bin/python

import sys
import os
import shutil

def prep():
    sys.stdout.write('Preparing chenli... ')
    sys.stdout.flush()
    working = 'working/'
    chen_src = 'data/chenli'
    chen_dst = working + 'chenli/'
    
    if not os.path.exists(working):
        os.mkdir(working)
    
    if os.path.exists(chen_dst):
        shutil.rmtree(chen_dst)
    os.mkdir(chen_dst)
    
    sentId = 0
    tokens = ''
    cors = ''
    for line in open(chen_src, 'r', encoding='utf-8', errors='ignore'):
        if len(line.split('\t')) > 2:
            tokens += line.split('\t')[0] + '\n'
            cors += line.split('\t')[2][:-1] + '\n'
        else:
            sentFolder = chen_dst + str(sentId) + '/'
            os.mkdir(sentFolder)
            open(sentFolder + 'orig', 'w').write(tokens)
            open(sentFolder + 'cor', 'w').write(cors)
            tokens = ''
            cors = ''
            sentId += 1
    
    sentFolder = chen_dst + str(sentId) + '/'
    os.mkdir(sentFolder)
    open(sentFolder + 'orig', 'w').write(tokens)
    open(sentFolder + 'cor', 'w').write(cors)
    
    shutil.rmtree(chen_dst + '0')
    sys.stdout.write('Done\n')
    sys.stdout.flush()
        
