#!/usr/bin/python

import sys
import os
import shutil

def prep():
    sys.stdout.write('Preparing lexnorm... ')
    sys.stdout.flush()
    
    working = 'working/'
    lexnorm_src = 'data/lexnorm'
    lexnorm_dst = working + 'lexnorm/'
    
    if not os.path.exists(working):
        os.mkdir(working)
    
    if os.path.exists(lexnorm_dst):
        shutil.rmtree(lexnorm_dst)
    os.mkdir(lexnorm_dst)
    
    sentId = 0
    tokens = ''
    cors = ''
    for line in open(lexnorm_src, 'r', encoding='utf-8', errors='ignore'):
        if len(line.split('\t')) > 2:
            tokens += line.split('\t')[0] + '\n'
            cors += line.split('\t')[2][:-1] + '\n'
        else:
            sentFolder = lexnorm_dst + str(sentId) + '/'
            os.mkdir(sentFolder)
            open(sentFolder + 'orig', 'w').write(tokens)
            open(sentFolder + 'cor', 'w').write(cors)
            tokens = ''
            cors = ''
            sentId += 1
    
    sentFolder = lexnorm_dst + str(sentId) + '/'
    os.mkdir(sentFolder)
    open(sentFolder + 'orig', 'w').write(tokens)
    open(sentFolder + 'cor', 'w').write(cors)
    
    shutil.rmtree(lexnorm_dst + '0')
    sys.stdout.write('Done\n')
    sys.stdout.flush()

