#!/usr/bin/python3

import json
import os
import shutil
import sys

def prep():
    sys.stdout.write('Preparing sms corpus... ')
    sys.stdout.flush()
    
    working = '../working/'
    sms_src = '../test/sms/en2cn-2k.en2nen2cn'
    sms_dst = working + 'sms/'
    
    if not os.path.exists(working):
        os.mkdir(working)
    
    if os.path.exists(sms_dst):
        shutil.rmtree(sms_dst)
    os.mkdir(sms_dst)
    
    sentCount = 0
    sentId = 1
    orig = ''
    cor = ''
    for line in open(sms_src):
        if sentCount % 3 == 0:
            orig = line
        elif sentCount % 3 == 1:
            cor = line
        else:
            idDir = sms_dst + str(sentId) + '/'
            os.mkdir(idDir)
    
            origFile = open(idDir + 'orig', 'w')
            for word in orig.split():
                origFile.write(word + '\n')
            origFile.close()
    
            corFile = open(idDir + 'cor', 'w')
            for word in cor.split():
                corFile.write(word + '\n')
            corFile.close()
    
            orig = ''
            cor = ''
            sentId += 1
        sentCount += 1

    sys.stdout.write('Done\n')
    sys.stdout.flush()

