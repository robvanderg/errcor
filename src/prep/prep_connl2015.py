#!/usr/bin/python3

import os
import shutil
import sys

def prep():
    sys.stdout.write('Preparing conll 2015... ')
    sys.stdout.flush()

    working = '../working/'
    conll_src = '../test/10gec_annotations/'
    conll_dst = working + 'conll2015/'
    
    if not os.path.exists (working):
        os.mkdir(working)
    
    if os.path.exists (conll_dst):
        shutil.rmtree(conll_dst)
    os.mkdir(conll_dst)
    
    allData = []
    for i in range(1, 11):
        src_file = conll_src + 'A' + str(i) + '.m2'
        sentId = 0
        orig = ''
        cor = ''
    
        for line in (open(src_file, 'r')):
            if len(line) < 2:
                if i is 1:
                    allData.append([orig, cor])
                else:    
                    allData[sentId][1] += cor
                orig = ''
                cor = ''
                sentId += 1
            else:
                if line[0] is 'S':
                    orig = line[2:].replace(' ', '\n')
                elif line[0] is 'A':
                    cor += line[2:-1] + str(i) + '\n'
    
    for i in range(len(allData)):
        sentFolder = conll_dst + str(i+1) + '/'
        os.mkdir(sentFolder)
    
        open(sentFolder + 'orig', 'w').write(allData[i][0])
        open(sentFolder + 'cor.m2', 'w').write(allData[i][1])

    sys.stdout.write('Done\n')
    sys.stdout.flush()

