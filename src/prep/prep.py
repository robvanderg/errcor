import prep.prep_chen as chen
import prep.prep_connl as connl
import prep.prep_connl2015 as connl2015
import prep.prep_lexnorm as lexnorm
import prep.prep_lexnorm2 as lexnorm2
import prep.prep_sms as sms

def prep():
    chen.prep()
    #connl.prep()
    #connl2015.prep()
    lexnorm.prep()
    #sms.prep()
