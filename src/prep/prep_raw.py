#!/usr/bin/python

import sys
import os
import shutil

def prep(src, name):
    sys.stdout.write('Preparing raw data: ' + src)
    sys.stdout.flush()
    
    working = 'working/'
    dst = working + name + '/'
    
    if not os.path.exists(working):
        os.mkdir(working)
    
    if os.path.exists(dst):
        shutil.rmtree(dst)
    os.mkdir(dst)
    
    sentId = 1
    for line in open(src, 'r', encoding='utf-8', errors='ignore'):
        sentFolder = dst + str(sentId) + '/'
        os.mkdir(sentFolder)
        orig = open(sentFolder + 'orig', 'w')
        for word in line.split():
            orig.write(word + '\n')
        orig.close()
        sentId += 1
    
    sys.stdout.write('Done\n')
    sys.stdout.flush()

