import numpy as np

def clean_ngram(ngr_loc):
    writeFile = open(ngr_loc + '.clean', 'w')
    counter = 0

    for line in open(ngr_loc, 'r'):
        #encoding='utf-8', errors='ignore'
        writeFile.write(line[:line.find('\t')] + '\t' + str(np.sum(np.array(line[:-1].split('\t')[1:], dtype=np.int)[::2])) + '\n')
    
        if counter % 100000 == 0:
            print (ngr_loc, counter)
        counter += 1

    writeFile.close()

if __name__ == '__main__':
    clean_ngram('data/tw.1')
    clean_ngram('data/tw.2')

