import os
import pickle

working = '../working/'
workingList = ['chenli', 'lexnorm']

def changeProt(working):
    for sentId in os.listdir(working):
        sentFolder = working + sentId + '/'
        data = pickle.load(open(sentFolder + 'asp.pickle', 'rb'))
        os.remove(sentFolder + 'asp.pickle')
        pickle.dump(data, open(sentFolder + 'asp.pickle', 'wb'), protocol=2)

for workingDir in workingList:
    workingDir = working + workingDir + '/'
    changeProt(workingDir)
