import os

working = '../working/chenli/'
found = {}
for sentId in os.listdir(working):
    sentFolder = working + sentId + '/'
    orig = ' '.join([line[:-1] for line in open(sentFolder + 'orig', 'r', encoding='utf-8', errors='ignore')])
    cor = ' '.join([line[:-1] for line in open(sentFolder + 'cor', 'r', encoding='utf-8', errors='ignore')])

    if orig in found:
        print(sentId, orig)
        print(sentId, orig)
    else:
        found[orig] = cor
