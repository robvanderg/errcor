import pickle
import os
working = '../working/chenli/' 

for sentId in os.listdir(working):
    sentFolder = working + sentId + '/'
    if (os.path.exists(sentFolder + 'asp2.pickle')):
        orig = [line[:-1] for line in open(sentFolder + 'orig', 'r')]
        cor = [line[:-1] for line in open(sentFolder + 'cor', 'r')]
        asp = pickle.load(open(sentFolder + 'asp.pickle', 'rb'))
        asp2 = pickle.load(open(sentFolder + 'asp2.pickle', 'rb'))
        w2v = pickle.load(open(sentFolder + 'w2v.pickle', 'rb'))
        for i in range(len(orig)):
            if orig[i].lower() != cor[i].lower():
                foundAsp = False
                for item in asp[i]:
                    if item[0].lower() == cor[i].lower():
                        foundAsp = True
                        break
                foundAsp2 = False
                for item in asp2[i]:
                    if item[0].lower() == cor[i].lower():
                        foundAsp2 = True
                        break
                foundW2V = False
                for item in w2v[i]:
                    if item[0].lower() == cor[i].lower():
                        foundW2V = True
                        break
                if not foundAsp and not foundW2V:
                    print(orig[i], cor[i])

