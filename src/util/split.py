import os
import random
import shutil

working = '../working/'
def copyFolder(src, dst):
    os.mkdir(dst)
    for datafile in os.listdir(src):
        shutil.copy(src + datafile, dst + datafile)

def split(ratio, corpus_name):
    corpus_dir = working + corpus_name + '/'
    dirs =  os.listdir(corpus_dir)
    random.shuffle(dirs)
    split = int(len(dirs) * ratio)

    train_folder = working + corpus_name + '_train/'
    os.mkdir(train_folder)
    for sentId in dirs[:split]:
        sentFolder = corpus_dir + sentId + '/'
        copyFolder(sentFolder, train_folder + sentId + '/')
        
    test_folder = working + corpus_name + '_test/'
    os.mkdir(test_folder)
    for sentId in dirs[split:]:
        sentFolder = corpus_dir + sentId + '/'
        copyFolder(sentFolder, test_folder + sentId + '/')

if __name__ == '__main__':
    split(.7, 'chenli')
