#!/usr/bin/python3

import os

working = '../working/'
dictList = ['aspell', 'scowl', 'hunspell', 'openoffice']
#workingList = [x + '/' for x in os.listdir('../working')]
workingList = ['chenli/']


def word2vec(workingDir):
    import features.w2v as w2v
    print 'find similar word vectors'
    w2v.findSims(workingDir)

if __name__ == '__main__':
    for workingDir in workingList:
        workingDir = working + workingDir
        word2vec(workingDir)
