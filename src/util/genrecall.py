import pickle
import os

working = '../working/chenli/' 
errors_c = 0
foundAsp_c = 0
foundAsp2_c = 0
foundW2v_c = 0
found_c = 0

for sentId in os.listdir(working):
    sentFolder = working + sentId + '/'
    if (os.path.exists(sentFolder + 'asp.pickle')):
        orig = [line[:-1] for line in open(sentFolder + 'orig', 'r')]
        cor = [line[:-1] for line in open(sentFolder + 'cor', 'r')]
        asp = pickle.load(open(sentFolder + 'asp.pickle', 'rb'))
        #asp2 = pickle.load(open(sentFolder + 'asp2.pickle', 'rb'))
        w2v = pickle.load(open(sentFolder + 'w2v.pickle', 'rb'))
        for i in range(len(orig)):
            if orig[i].lower() != cor[i].lower():
                foundAsp = False
                for item in asp[i]:
                    if item[0].lower() == cor[i].lower():
                        foundAsp = True
                        break
                #foundAsp2 = False
                #for item in asp2[i]:
                #    if item[0].lower() == cor[i].lower():
                #        foundAsp2 = True
                #        break
                foundW2V = False
                for item in w2v[i]:
                    if item[0].lower() == cor[i].lower():
                        foundW2V = True
                        break
                if foundAsp:
                    foundAsp_c += 1
                #if foundAsp2:
                #    foundAsp2_c += 1
                if foundW2V:
                    foundW2v_c += 1
                if foundAsp or foundW2V:
                    found_c += 1
                else:
                    #print(orig[i], cor[i])
                    pass
                errors_c += 1
       
print(errors_c)
print('asp', foundAsp_c, foundAsp_c / float(errors_c)) 
#print('asp2', foundAsp2_c, foundAsp2_c / float(errors_c)) 
print('w2v', foundW2v_c, foundW2v_c / float(errors_c)) 
print('comb', found_c, found_c / float(errors_c)) 
