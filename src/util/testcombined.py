import pickle
import os

import featnames 
from random import shuffle 

def feature_rank(candList, args):
    shuffle(candList)
    candList.sort(key=lambda l:l[args[0]], reverse=args[1])
    return sorted(candList, key=lambda l:l[0], reverse=True)

feat_names = featnames.get()

def getX(data_dict):
    data_list = []
    for feat_name in feat_names:
        if feat_name in data_dict:
            if data_dict[feat_name] == True:
                data_list.append(1)
            elif data_dict[feat_name] == False:
                data_list.append(0)
            else:
                data_list.append(data_dict[feat_name])
        else:
            data_list.append(0)
    return data_list

working = '../working/chenli/' 
errors = 0
found = 0

for sentId in os.listdir(working):
    sentFolder = working + sentId + '/'
    orig = [line[:-1] for line in open(sentFolder + 'orig', 'r')]
    cor = [line[:-1] for line in open(sentFolder + 'cor', 'r')]
    combined = pickle.load(open(sentFolder + 'combined.pickle', 'rb'))
    for i in range(len(orig)):
        if orig[i].lower() != cor[i].lower():
            found = False
            candList = []
            for item in combined[i]:
                candList.append(getX(combined[i][item]))
            candList = feature_rank(candList, [2, False])
            for item in candList:
                print(item[2])
            
            
       
print(errors)
print('res', found, found / float(errors)) 
