import pickle
import sys
import os 

def coords(positions, tres):
    count_list = []
    for i in range(1,tres+1):
        count_list.append(positions.count(i))
    cum = 0
    for i in range(0,tres):
        cum += count_list[i]
        count_list[i] = cum / len(positions)

    return count_list

working = '../working/chenli/'
foundIdxs = []

for sentId in os.listdir(working)[int(len(os.listdir(working))*0.7):]:
    sentFolder = working + sentId + '/'
    data = pickle.load(open(sentFolder + 'asp2.pickle', 'rb'))
    orig = [line[:-1] for line in open(sentFolder + 'orig', 'r')]
    cor = [line[:-1] for line in open(sentFolder + 'cor', 'r')]
    for i in range(len(orig)):
        if orig[i].lower() == cor[i].lower():
            foundIdxs.append(1)
        else:
            foundIdx = 0
            for candIdx in range(len(data[i])):
                if data[i][candIdx][0].lower() == cor[i].lower():
                   foundIdx = candIdx + 1
                   break   
            foundIdxs.append(foundIdx)
pickle.dump(coords(foundIdxs, 1500), open('../results_dev/oldAsp.pickle', 'wb'))
        
