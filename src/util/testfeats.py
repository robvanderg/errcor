import pickle

feat_names = ['self', 'asp1', 'asp2', 'w2v1', 'w2v2', 'found_w2v', 'aspell', 'scowl', 'hunspell', 'openoffice']

for name in ['t_ngr.1', 't_ngr.2b', 't_ngr.2a', 'g_ngr.1', 'g_ngr.2b', 'g_ngr.2a']:
    feat_names += [name + 'f.bool', name + 'f', name + 'p']
#for name in ['aspell', 'scowl', 'openoffice', 'hunspell', 't_ngr.1f.bool', 'g_ngr.1f.bool']:
#    feat_names += [name + '.io', name + '.ii', name + '.oi', name + '.oo']

data = pickle.load(open('test.pickle', 'rb'))

zeros = []
for name in feat_names:
    zeros.append(0)

for dataPoint in data:
    for featIdx in range(len(dataPoint)):
        if dataPoint[featIdx] != 0:
            zeros[featIdx] += 1

for i in range(len(feat_names)):
    print (feat_names[i], zeros[i])

