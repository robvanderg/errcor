#!/usr/bin/python3

import os
import pickle
from gen.word2vecReader import Word2Vec as Word2Vec
import sys

def rm_number(word):
    newword = ''
    for char in word:
        if char.isdigit():
            newword += '_NUM_'
        else:
            newword += char
    return newword

class w2v():
    def __init__(self, loc):
        sys.stdout.write('Loading model... ' + loc)
        sys.stdout.flush()
        self.model = Word2Vec.load_word2vec_format(loc, binary=True)
        sys.stdout.write('Done\n')
        sys.stdout.flush()
    
    def findSims(self, working):
        sys.stdout.write('Finding similar vectors... ')
        sys.stdout.flush()
        for sentId in os.listdir(working):
            sentFolder = working + sentId + '/'
            if not os.path.exists(sentFolder + 'w2v.pickle'):
                orig = [line[:-1] for line in open(sentFolder + 'orig', 'r')]
            
                data = []
                for token in orig:
                    token = rm_number(token)
                    token_data = []
                    if self.model.__contains__(token):
                        for token_stats in self.model.most_similar(token, [], 500):
                            token_data.append([token_stats[0], token_stats[1]])
                    data.append(token_data)
                pickle.dump(data, open(sentFolder + 'w2v.pickle','wb'), protocol=2)
        sys.stdout.write('Done\n')
        sys.stdout.flush()
        sys.stdout.flush()
            
