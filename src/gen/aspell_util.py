#/usr/bin/python3

import os

char2str = {'a':'aa',
            'b':'bee',
            'c':'cee',
            'd':'dee',
            'e':'ee',
            'f':'ef',
            'g':'gee',
            'h':'aitch',
            'i':'ii',
            'j':'jay',
            'k':'kay',
            'l':'el',
            'm':'em',
            'n':'en',
            'o':'oo',
            'p':'pee',
            'q':'cue',
            'r':'ar',
            's':'ess',
            't':'tee',
            'u':'ju',
            'v':'vee',
            'w':'doubleju',
            'x':'ex',
            'y':'wy',
            'z':'zed', # no see?
            }

int2str = { '0': ['zero', 'o'], 
            '1': ['one', 'i'], 
            '2':['two'], 
            '3':['three', 'e'],
            '4':['four', 'a'],
            '5':['five', 's'],
            '6':['six'], #Ook g? in towards facilitating ...
            '7':['seven' 't'],
            '8':['eight'],
            '9':['nine', 'g']
          }
def repl_following(word):
    secondLastChar = ''
    lastChar = ''
    newword = ''
    for char in word:
        if char == lastChar and lastChar == secondLastChar:
            pass
        else:
            newword+= char
        secondLastChar = lastChar
        lastChar = char
    return newword

def repl(word):
    word1 = ''
    word2 = ''
    two_repl = False
    for char in word:
        if char.isdigit():
            word1 += int2str[char][0]
            if (len(int2str[char]) > 1):
                two_repl = True
                word2 += int2str[char][1]
        elif char.isalpha():
            word1 += char
            word2 += char
    if two_repl:
        return [word1, word2]
    else:
        return [word1]

def normalize(word):
    word = word.lower()
    word = repl_following(word) 
    newwords = ['']
    if len(word) == 1 and word.isalpha():
        return [char2str[word.lower()]]
    for char in word: 
        if char.isalpha(): 
            newwords[0] += char 
    if any(char.isdigit() for char in word):
        newwords += repl(word)
    return newwords


def aspell(word, normalize_b):
    results = []
    run_on = []
    if normalize_b:
        run_on = normalize(word)
    else:
        run_on = [word]
    for alt_word in run_on:
        cmd = os.popen('bash suglist.sh ' + alt_word)
        idx = 1
        for line in cmd.readlines():
            tabLoc = line.find('\t')
            score = int(line[:tabLoc])
            word = line[tabLoc+1:-1]
            newRes = True
            for oldRes in results:
                if oldRes[0] == word:
                    newRes = False
                    break
            if newRes:
                results.append([word, score, idx])
            idx += 1
        addAlt = True
        for oldRes in results:
            if oldRes[0] == alt_word:
                addAlt = False
                break
        if addAlt:
            results.append([alt_word, 0, 0])
            
    return results
