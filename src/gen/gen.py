import gen.w2v as w2v
import gen.aspell as asp

def gen(working):
    twitter_embeddings = w2v.w2v('data/word2vec_twitter_model.bin')
    twitter_embeddings.findSims(working)
    asp.findSims(working)

