#/usr/bin/python3

import os
import pickle
import sys
import gen.aspell_util as aspell_util

def findSims(working):
    sys.stdout.write('Finding similar words with aspell...')
    sys.stdout.flush()
    for sentId in os.listdir(working):
        sentFolder = working + sentId + '/'
        orig = [line[:-1] for line in open(sentFolder + 'orig', 'r', encoding='utf-8', errors='ignore')]
        data = []
        for token in orig:
            data.append(aspell_util.aspell(token, True))
        pickle.dump(data, open(sentFolder + 'asp.pickle', 'wb'), protocol=2)
    
    sys.stdout.write('Done\n')
    sys.stdout.flush()


