import os
import sys
import pickle
import rank.forest as forest
import rank.featnames as feats
from sklearn.ensemble import RandomForestClassifier
from operator import itemgetter

def getX(data_dict):
    data_list = []
    for feat_name in feats.get():
        if feat_name in data_dict:
            if data_dict[feat_name] == True:
                data_list.append(1)
            elif data_dict[feat_name] == False:
                data_list.append(0)
            else:
                data_list.append(data_dict[feat_name])
        else:
            if feat_name in ['asp1', 'asp2', 'w2v2']:
                data_list.append(999)
            else:
                data_list.append(0)
    return data_list

def getFreqs():
    freqs = {}
    for line in open('data/train.freq2'):
        freqs[line.split()[1].strip()] = int(line.split()[0].strip()) 
    return freqs

def write(testDir, numCands, prob, filename):
    sys.stdout.write("Writing results  " + filename + str(numCands) + str(prob))
    sys.stdout.flush()
    rf = pickle.load(open('../50/rf.pickle', 'rb'))
    freqs = getFreqs()
    names = set(line.strip().lower() for line in open('data/names'))
    output = open('output/' + filename + str(numCands) + str(prob), 'w')
    for sentId in range(1, len(os.listdir(testDir))+ 1):
        sentFolder = testDir + str(sentId) + '/'
        combined = pickle.load(open(sentFolder + 'combined.pickle', 'rb'))
        for i in range(len(combined)):
            wordData = []
            for cand in combined[i]:
                wordData.append(getX(combined[i][cand]) + [cand])
            result = forest.rank(wordData, rf)
            origWord = result[0][2]
            if (not any(c.isalpha() for c in origWord)) or origWord == 'Urlname' or origWord == 'Username':
                output.write(str(i) + ' ' + origWord + ' ' + str(i+1) + ' 1.0\n')
            #elif (origWord.lower() in names):
            #    output.write(str(i) + " JRCNAME " + str(i+1) + ' 1.0\n')
            else:
                sortedCands = result[1:numCands + 1]
                
                freq = 1
                if origWord in freqs:
                    freq += freqs[origWord]
                origProb = freq * prob
                if (origProb > 0.9):
                    origProb = 0.9
                output.write(str(i) + ' ' + origWord + ' ' + str(i+1) + ' ' + 
                             str(origProb) + '\n')
                
                totalConfCands = 0.0
                for item in sortedCands:
                    totalConfCands += item[1]
                for item in sortedCands:
                    candProb = (1 - origProb) * (item[1] / totalConfCands)
                    output.write(str(i) + ' ' + item[2] + ' ' + str(i+1) + ' ' +
                            str(candProb) + '\n')
                    
               
        output.write('.\n')
    output.close()

