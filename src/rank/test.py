import pickle
import sys

def coords(positions, tres):
    count_list = []
    for i in range(1,tres+1):
        count_list.append(positions.count(i))

    cum = 0
    for i in range(0,tres):
        cum += count_list[i]
        count_list[i] = cum / len(positions)
    
    return [0] + count_list

def test(X, y, tres, sortFunction, functionArg):
    foundIdxs = []
    counter = 0
    for sentIdx in range(len(X)):
        counter = counter +1
        sys.stdout.write('Processing sent: ' + str(counter) + '\n')
        sys.stdout.flush()
        for wordIdx in range(len(X[sentIdx])):
            cor = y[sentIdx][wordIdx]
            rankedCands = sortFunction(X[sentIdx][wordIdx], functionArg)
            found = False
            for candIdx in range(0,len(rankedCands)):                
                if rankedCands[candIdx][len(rankedCands[candIdx])-1].lower() == cor.lower():
                    found = True
                    if rankedCands[candIdx][0] == 1:
                        #print("Not changed: ", cor) 
                        pass
                    else:
                        print('found: ',rankedCands[0][len(rankedCands[0])-1], cor)
                        foundIdxs.append(candIdx)
                    break
            if not found:
                foundIdxs.append(tres)
                print('Not found: ',rankedCands[0][len(rankedCands[0])-1], cor)
    print (len(foundIdxs))
    return coords(foundIdxs, tres)

