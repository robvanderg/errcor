import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import datetime

def transform(coords):
    newcoords = []
    for coord in coords:
        newcoords.append( (coord - 0.88) * (1/0.12) )
    return newcoords

def graph(coords, names, tres1, tres2, show):
    
    styles = ['-','--','-.',':']
    styles = styles + styles
    styles = styles + styles
    styles = styles + styles
    styles = styles + styles
    styles = styles + styles
    styles = styles + styles
   
    for lineIdx in range(len(coords)):
        plt.plot(coords[lineIdx][0], linestyle=styles[lineIdx], label=names[lineIdx], )
        print(names[lineIdx], coords[lineIdx][0][1])
        print(names[lineIdx], coords[lineIdx][0][3])
        print(names[lineIdx], coords[lineIdx][0][10])
        print(names[lineIdx], coords[lineIdx][0][20])
        print(names[lineIdx], coords[lineIdx][0][tres1])

    plt.legend(loc='lower right')
    plt.ylim((tres2,1))
    plt.xlim((0,tres1))
    plt.grid(True)
    plt.xlabel('Number of candidates returned ')
    plt.ylabel('Recall (% found corrections)')
    plt.savefig(str(datetime.datetime.now()) + '.png')
    if show:
        plt.show()
    
    plt.clf()
    #plt.set_yticks([x * 0.1 for x in range(0,11)])
    #plt.set_xticks([x * tres/10 for x in range(0,11)])
    
