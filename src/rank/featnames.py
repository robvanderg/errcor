

def get():
    feat_names = ['self']

    #aspell
    feat_names += ['asp1', 'asp2','aspell', 'scowl', 'hunspell']
    for name in ['aspell', 'scowl', 'openoffice', 'hunspell', 't_ngr.1f.bool', 'g_ngr.1f.bool']:
        feat_names += [name + '.io', name + '.ii', name + '.oi', name + '.oo']
    
    #w2v
    feat_names += ['w2v1', 'w2v2', 'found_w2v']

    #ngrams
    for name in ['t_ngr.1', 't_ngr.2b', 't_ngr.2a', 'g_ngr.1', 'g_ngr.2b', 'g_ngr.2a']:
        feat_names += [name + 'p', name + 'f', name + 'f.bool']

    return feat_names

if __name__ == '__main__':
    feat_names = get()
    for i in range(len(feat_names)):
        print(i, feat_names[i])
