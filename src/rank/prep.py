import os
import pickle
import sys
from sklearn.linear_model import LogisticRegression
import numpy as np
import rank.featnames as feats

feat_names = feats.get()

def getX(data_dict):
    data_list = []
    for feat_name in feat_names:
        if feat_name in data_dict:
            if data_dict[feat_name] == True:
                data_list.append(1)
            elif data_dict[feat_name] == False:
                data_list.append(0)
            else:
                data_list.append(data_dict[feat_name])
        else:
            if feat_name in ['asp1', 'asp2', 'w2v2']:
                data_list.append(999)
            else:
                data_list.append(0)
    return data_list

def prepTraining(working, sentIds, filename, makeSmaller):
    sys.stdout.write("preprocessing: " + filename + "... \t")
    sys.stdout.flush()
    X = [] 
    y = []
    for sentId in sentIds:
        sentFolder = working + sentId + '/'
        cor = [line[:-1] for line in open(sentFolder + 'cor', 'r', encoding='utf-8', errors='ignore')]
        orig = [line[:-1] for line in open(sentFolder + 'orig', 'r', encoding='utf-8', errors='ignore')]

        combined = pickle.load(open(sentFolder + 'combined.pickle', 'rb'))
        for i in range(len(cor)):
            if not makeSmaller or cor[i].lower() != orig[i].lower():  
                for cand in combined[i]:
                    if cand.lower() == cor[i].lower():
                        y.append(1)
                    else:
                        y.append(0)
                    X.append(getX(combined[i][cand]))
    
    X_small = np.array(X, dtype=np.float32)
    y_small = np.array(y, dtype=np.int8)
    with open(filename,'wb') as pickleFile:   
        pickle.dump(X_small, pickleFile)
        pickle.dump(y_small, pickleFile)
    sys.stdout.write("done, with " + str(len(X)) + " instances, and " + str(len(feat_names)) + ' features\n')
    sys.stdout.flush()

def prepTesting(working, sentIds, filename):
    sys.stdout.write("preprocessing: " + filename + "... \t")
    sys.stdout.flush()
    X = []
    y = []
    idx = 0
    for sentId in sentIds:
        sentFolder = working + sentId + '/'
        cor = [line[:-1] for line in open(sentFolder + 'cor', 'r', encoding='utf-8', errors='ignore')]
        combined = pickle.load(open(sentFolder + 'combined.pickle', 'rb'))
        sentData = []
        for i in range(len(combined)):
            wordData = []
            for cand in combined[i]:
                wordData.append(getX(combined[i][cand]) + [cand])
            sentData.append(wordData)
        X.append(sentData)
        y.append(cor)
    with open(filename,'wb') as pickleFile:   
        pickle.dump(X,pickleFile)
        pickle.dump(y,pickleFile)
    sys.stdout.write("done, with " + str(len(X)) + " instances, and " + str(len(feat_names)) + ' features\n')
    sys.stdout.flush()
