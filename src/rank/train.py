import pickle
import sys
from sklearn.ensemble import RandomForestClassifier

def trainRF(src, dst):
    sys.stdout.write("loading train data...")
    sys.stdout.flush()
    with open(src, 'rb') as readfile:
        X = pickle.load(readfile)
        y = pickle.load(readfile)
        sys.stdout.write("Done\n")
        sys.stdout.flush()
        rf = RandomForestClassifier(n_estimators=100, n_jobs=8)
        rf.fit(X,y)
        pickle.dump(rf, open(dst, 'wb'))

