import numpy as np
from sklearn.ensemble import RandomForestClassifier

def rank(candList, lr):
    features = [item[:len(candList[0])-1] for item in candList]
    names = [item[len(candList[0])-1:] for item in candList]
    result = lr.predict_proba(features)
    rankList = []
    for idx in range(len(features)):
        rankList.append( [features[idx][0], result[idx][1], names[idx][0]])
    rankList.sort(key=lambda l:l[1], reverse=True)
    return sorted(rankList, key=lambda l:l[0], reverse=True)
