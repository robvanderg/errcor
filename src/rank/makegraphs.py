import graph 
import graph2
import pickle


def makeGraph(feats, names, tres, folder):
    coords = []
    for feat in feats:
        feat_data = pickle.load(open(folder + feat + '.pickle', 'rb'))
        coords.append((feat_data, feat))
    graph.graph(coords, names, tres, 0, False)

def makeGraph2(feats, names, tres):
    coords = []
    for feat in feats:
        feat_data = pickle.load(open(feat + '.pickle', 'rb'))
        coords.append((feat_data, feat))
    graph.graph(coords, names, 40, 0.8, False)
    
def makeGraph3():
    tr_lines = ['-001', '-005','-01', '-02', '-03', '-04' ,'-05', '-06', '-07', '-08', '-09','']
    test_results = []
    for i in range(len(tr_lines)):
        test_results.append(pickle.load(open('../50' + tr_lines[i] + '/results_test/rf.pickle', 'rb')))
    cand_names = [1,3,10,20]
    cand_lines = []
    for candNum in cand_names:
        cand_line = []
        for results in test_results:
            cand_line.append(results[candNum])
        cand_lines.append(cand_line)
        print(candNum, cand_line)
    cand_names = ['1 cand', '3 cands', '10 cands', '20 cands']
    graph2.graph(cand_lines, cand_names, 10, False)

if __name__ == '__main__':
    data_folder = '../50/results_test/'
    makeGraph(['rf'],['Random Forest'], 500, data_folder)
    #makeGraph(['w2v2', 'asp2', 'combination'],['Word Embeddings', 'Aspell', 'Combination'], 500, data_folder)
    #makeGraph(['w2v2', 'asp2', 'g_ngr.1p', 't_ngr.1p', 'rf'],['Word Embeddings', 'Aspell','Google Unigrams','Twitter Unigrams', 'Random Forest'], 200, data_folder)
 
    #makeGraph2(['../50/results_test/rf', '../50-w2v/results_test/rf', '../50-asp2/results_test/rf', '../50-ngr/results_test/rf'], ['all', '-word embeddings', '-aspell', '-ngrams'], 100)   
    #makeGraph3()
