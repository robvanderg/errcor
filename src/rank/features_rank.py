from random import shuffle 

def feature_rank(candList, args):
    candList2 = candList
    shuffle(candList)
    shuffle(candList2)
    candList.sort(key=lambda l:l[1], reverse=False)
    candList2.sort(key=lambda l:l[3], reverse=True)
    newCandList = []
    end = len(candList[0])-1
    for oldIdx in range(len(candList)):
        newAsp = True
        newW2V = True
        for newIdx in range(len(newCandList)):
            if newCandList[newIdx][end] == candList[oldIdx][end]:
                newAsp = False
            if newCandList[newIdx][end] == candList2[oldIdx][end]:
                newW2V = False
        if candList2[oldIdx][end] == candList[oldIdx][end]:
            newCandList.append(candList[oldIdx])
        else:
            if newAsp:
                newCandList.append(candList[oldIdx])
            if newW2V:
                newCandList.append(candList2[oldIdx])
    return sorted(candList, key=lambda l:l[0], reverse=True)

