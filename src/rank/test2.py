import os
import sys
import pickle



def test(testDir):
    found = 0
    notFound = 0
    errors = 0
    for sentId in os.listdir(testDir):
        sentFolder = testDir + sentId + '/'
        if os.path.exists(sentFolder + 'output'): #TODO can be removed
            cor = [line[:-1] for line in open(sentFolder + 'cor', 'r', encoding='utf-8', errors='ignore')]
            orig = [line[:-1] for line in open(sentFolder + 'orig', 'r', encoding='utf-8', errors='ignore')]
            output = [line.split('::') for line in open(sentFolder + 'output', 'r', encoding='utf-8', errors='ignore')]
            for i in range(len(cor)):
                if len(output) == len(cor): #TODO can be removed
                    foundThisCor = False
                    if orig[i] != cor[i]:
                        errors += 1
                    for cand in output[i]:
                        if cand.lower() == cor[i].lower():
                            foundThisCor = True
                    if foundThisCor:
                        found += 1
                    else:
                        notFound += 1
    
    total = found + notFound
    rec = float(found) / total
    prec = 1- (float(notFound)/errors)
    sys.stdout.write('found: ' + str(found) + '\n')
    sys.stdout.write('Nfound: ' + str(notFound) + '\n')
    sys.stdout.write('errors: ' + str(errors) + '\n')
    sys.stdout.write('\n')
    sys.stdout.write('Recall: ' + str(rec) + '\n')
    sys.stdout.write('Precision: ' + str(prec) + '\n')
    sys.stdout.flush
