from random import shuffle 

def feature_rank(candList, args):
    shuffle(candList)
    candList.sort(key=lambda l:l[args[0]], reverse=args[1])
    return sorted(candList, key=lambda l:l[0], reverse=True)

