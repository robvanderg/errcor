import os
import sys
import pickle

import rank.prep as prep
import rank.testall as test
import rank.train as train

def prepData(train_data, test_data, testDir):
    split = int(len(os.listdir(train_data)) * 1)
    train_ids = os.listdir(train_data)[:split]
    dev_ids = os.listdir(train_data)#[split:]
    test_ids = os.listdir(test_data)
    prep.prepTraining(train_data, train_ids, testDir + 'train.pickle', True)
    #prep.prepTesting(train_data, dev_ids, testDir + 'dev.pickle')
    prep.prepTesting(test_data, test_ids, testDir + 'test.pickle')
    pass

def rank(train_data, test_data):
    testDir = '../50/'
    if not os.path.exists(testDir):
        os.mkdir(testDir)
    #prepData(train_data, test_data, testDir) 
    train.trainRF(testDir + 'train.pickle', testDir + 'rf.pickle')

    test.saveScores(testDir + 'test.pickle', testDir + 'results_test/', testDir)
    pass
     
