import sys
import pickle
import os

import rank.test as test
import rank.graph as graph
import rank.feature_rank as fr
import rank.features_rank as fr2
import rank.logres as logres
import rank.forest as forest
import rank.featnames as feats

def saveScores(testLoc, dst, testDir):
    if not os.path.exists(dst):
        os.mkdir(dst)
    sys.stdout.write("loading test data...")
    sys.stdout.flush()
    X = []
    y = []
    with open(testLoc, 'rb') as readfile:
        X = pickle.load(readfile)
        y = pickle.load(readfile)
    sys.stdout.write("Done\n")
    sys.stdout.flush()
    
    tres = 1500
    feat_names = feats.get()
    coords = []
#    for i in range(len(feat_names)):
#%        reverse = True
#        if feat_names[i] in ['asp1', 'asp2', 'w2v2']:
#            reverse = False
#        pickle.dump(test.test(X, y, tres, fr.feature_rank, [i, reverse]), open(dst + feat_names[i] + '.pickle', 'wb') )
    pickle.dump(test.test(X, y, tres, fr2.feature_rank, []), open(dst + 'combination.pickle', 'wb'))
    rf = pickle.load(open(testDir + 'rf.pickle','rb'))
    pickle.dump(test.test(X, y, tres, forest.rank, rf), open(dst + 'rf.pickle', 'wb') )

