import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import datetime

def transform(coords):
    newcoords = []
    for coord in coords:
        newcoords.append( (coord - 0.88) * (1/0.12) )
    return newcoords

def graph(coords, names, tres, show):
    styles = ['-','--','-.',':']
    styles = styles + styles
    styles = styles + styles
    styles = styles + styles
    styles = styles + styles
    styles = styles + styles
    styles = styles + styles
   
    for lineIdx in range(len(coords)):
        y = [1, 5, 10,20,30,40,50,60,70,80,90,100]
        x = coords[lineIdx]
        print(len(y), len(x))
        plt.plot(y,x, linestyle=styles[lineIdx], label=names[lineIdx], )

    plt.legend(loc='upper left')
    plt.ylim((0.8,1))
    plt.xlim((0,100))
    plt.grid(True)
    plt.xlabel('Amount of training data (in % of 2577 Sentences) ')
    plt.ylabel('Recall (% found corrections)')
    plt.savefig(str(datetime.datetime.now()) + '.png')
    if show:
        plt.show()
    
    plt.clf()
    #plt.set_yticks([x * 0.1 for x in range(0,11)])
    #plt.set_xticks([x * tres/10 for x in range(0,11)])
    
