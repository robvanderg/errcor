grep -iv ^$1$ ./data/dict/dict > tmp

rm -f ./data/dict/en-common.rws

~/aspell/aspell --lang=en create master ./data/dict/en-common.rws < ./tmp

rm -f ./tmp

echo $1 | ~/aspell/aspell -a list --data-dir=./data/dict --sug-mode=bad-spellers | grep ^[0-9] --color=never | uniq
