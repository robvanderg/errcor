import feats.combine as combine
import feats.simple_feats as simple
import feats.ngram_utils as ngr_util
import feats.add_ngrams as ngr_add
import os
import sys

twitter_loc = 'data/twitter.pickle'
google_loc = 'data/google.pickle'

def prepNgrams():
    if not os.path.exists(twitter_loc):
        ngr_util.write('data/tw.1.clean', 'data/tw.2.clean', twitter_loc)
    if not os.path.exists(google_loc):
        ngr_util.write('data/go.1', 'data/go.2', google_loc)

def feats(working):
    combine.combine(working)
    prepNgrams()
    ngr_add.add(working, twitter_loc, 't_ngr')
    ngr_add.add(working, google_loc, 'g_ngr')
    simple.addSimple(working)

