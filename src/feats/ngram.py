from scipy.sparse import lil_matrix
import pickle
import numpy as np

class ngram():
    def init(self, src):
        with open(src, 'rb') as f:
            self.voc = pickle.load(f)
            self.unigr_counts = pickle.load(f)
            self.bigr_counts = pickle.load(f)
        self.total_freqs = float(np.sum(self.unigr_counts))

    def getUniFreq(self, token):
        if token not in self.voc:
            return 0
        return self.unigr_counts[self.voc[token]]

    def getUniProb(self, token):
        if token not in self.voc:
            return 0
        return self.unigr_counts[self.voc[token]] / self.total_freqs
    
    def getBiFreq(self, token):
        split = token.find(' ')
        token1 = token[:split]
        token2 = token[split+1:]
        if token1 not in self.voc or token2 not in self.voc:
            return 0
        return self.bigr_counts[self.voc[token1],self.voc[token2]]

    
    def getBiProb(self, token1, token2, prob_token1):
        if token1 not in self.voc or token2 not in self.voc:
            return 0.0
        bigr_freq = float(self.bigr_counts[self.voc[token1], self.voc[token2]])
        if bigr_freq == 0.0:
            return 0
        if prob_token1:
            return bigr_freq / self.unigr_counts[self.voc[token1]]
        else:
            return bigr_freq / self.unigr_counts[self.voc[token2]]
