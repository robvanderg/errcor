import os
import pickle
import feats.ngram as ngr
import sys

def addFeat(token_data, cand, feat_name, val):
    if len(cand) == 0:
        return
    if cand not in token_data:
        token_data[cand] = {'token': cand, feat_name : val}
    else:
        token_data[cand][feat_name] = val

def add_aspell(token_data, asp):
    for cand in asp:
        addFeat(token_data, cand[0], 'asp1', cand[1])
        addFeat(token_data, cand[0], 'asp2', cand[2])

def add_w2v(token_data, w2v):
    idx = 1
    for cand in w2v:
        addFeat(token_data, cand[0], 'w2v1', cand[1])
        addFeat(token_data, cand[0], 'w2v2', idx)
        idx += 1

def combine(working):
    sys.stdout.write('Combining features...')
    sys.stdout.flush()
    for sentId in os.listdir(working):
        sentFolder = working + sentId + '/'
        orig = [line[:-1] for line in open(sentFolder + 'orig', 'r')]
        asp = pickle.load(open(sentFolder + 'asp.pickle', 'rb'))
        w2v = pickle.load(open(sentFolder + 'w2v.pickle', 'rb'))
        data = []
        for i in range(len(orig)):
            token_data = {} # token_data = dict containing dicts for each candidate
            token_data[orig[i]] = {'token' : orig[i], 'self' : 1}
            add_aspell(token_data, asp[i])
            add_w2v(token_data, w2v[i])
            data.append(token_data)
        pickle.dump(data, open(sentFolder + 'combined.pickle', 'wb'))
    
    sys.stdout.write('Done\n')
    sys.stdout.flush()
