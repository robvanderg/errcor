import numpy as np
from scipy.sparse import lil_matrix
import pickle
import sys
#TODO include cleaning process here
def write(unigrams_path, bigrams_path, dest):
    voc_size = 0
    voc = {}
    unigr_counts = []

    sys.stdout.write(dest + '\n')
    sys.stdout.write('Reading unigrams... ')
    sys.stdout.flush()
    for line in open(unigrams_path):
        split = line.find('\t')
        voc[line[:split]] = voc_size
        unigr_counts.append(int(line[split+1:]))
        voc_size += 1
    sys.stdout.write('done\n')
    
    sys.stdout.write('Reading bigrams... ')
    sys.stdout.flush()
    bigr_counts = lil_matrix((voc_size, voc_size), dtype=np.int32)
    for line in open(bigrams_path):
        split1 = line.find(' ')
        split2 = line.find('\t')
        try:
            id1 = voc[line[:split1]]
            id2 = voc[line[split1+1:split2]]
            bigr_counts[id1,id2] = int(line[split2+1:])
        except KeyError: # boundary == <S> </S>
            pass
    sys.stdout.write('done\n') 
    
    sys.stdout.write('writing:\n')   
    sys.stdout.flush()
    with open(dest, 'wb') as f: 
        sys.stdout.write('voc... ')
        sys.stdout.flush()
        pickle.dump(voc, f)

        sys.stdout.write('done\n unigrams... ')
        sys.stdout.flush()
        pickle.dump(np.asarray(unigr_counts), f)

        sys.stdout.write('done\n bigrams... ')
        sys.stdout.flush()
        pickle.dump(bigr_counts, f)
        sys.stdout.write('done\n')
        sys.stdout.flush()

