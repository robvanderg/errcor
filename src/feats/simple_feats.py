import sys
import pickle
import os
import feats.dict_feats as dict_feats

def addFeat(token_data, cand, feat_name, val):
    if len(cand) == 0: #WHY?
        return
    else:
        token_data[cand][feat_name] = val

def add_dicts(token_data):
    dicts = dict_feats.getNames()
    for cand in token_data:
        results = dict_feats.find(cand)
        for i in range(len(dicts)):
            addFeat(token_data, cand, dicts[i], results[i])

def add_bools(token_data):
    for cand in token_data:
        addFeat(token_data, cand, 'found_w2v', 'w2v2' in token_data[cand])
        for ngr_feat in ['t_ngr.1f', 't_ngr.2bf', 't_ngr.2af', 'g_ngr.1f', 'g_ngr.2bf', 'g_ngr.2af']:
            if ngr_feat in token_data[cand]:
                addFeat(token_data, cand, ngr_feat + '.bool', token_data[cand][ngr_feat] == 0)

def orig_feats(token_data, orig_data):
    for cand in token_data:
        for foundName in ['aspell', 'scowl', 'openoffice', 'hunspell', 't_ngr.1f.bool', 'g_ngr.1f.bool']:
            if foundName not in orig_data:
                print('  notFound')
                break
            orI = orig_data[foundName]
            caI = token_data[cand][foundName]
            addFeat(token_data, cand, foundName + '.io', orI == True and caI == False)
            addFeat(token_data, cand, foundName + '.ii', orI == True and caI == True)
            addFeat(token_data, cand, foundName + '.oi', orI == False and caI == False)
            addFeat(token_data, cand, foundName + '.oo', orI == False and caI == True)
    #TODO add diff in prob ngrams?
 
def addSimple(working):
    sys.stdout.write('Adding simple features... ')
    sys.stdout.flush()
    for sentId in os.listdir(working):
        print(sentId)
        sentFolder = working + sentId + '/'
        orig = [line[:-1] for line in open(sentFolder + 'orig', 'r')]
        sent_data = pickle.load(open(sentFolder + 'combined.pickle', 'rb'))
        for i in range(len(orig)):
            add_dicts(sent_data[i])
            add_bools(sent_data[i])
            orig_feats(sent_data[i], sent_data[i][orig[i]])
        pickle.dump(sent_data, open(sentFolder + 'combined.pickle', 'wb'))
    sys.stdout.write('Done\n')
    sys.stdout.flush()

