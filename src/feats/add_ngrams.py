import os
import pickle
import sys
import feats.ngram as ngr

ngrMod = ngr.ngram()

def procSent(data, orig, feat_name):
    for i in range(len(orig)):
        for cand in data[i]:
            data[i][cand][feat_name + '.1p'] = ngrMod.getUniProb(cand)
            data[i][cand][feat_name + '.1f'] = ngrMod.getUniFreq(cand)
            if i != 0:
                data[i][cand][feat_name + '.2bp'] = ngrMod.getBiProb(orig[i-1], cand, False)
                data[i][cand][feat_name + '.2bf'] = ngrMod.getBiFreq(orig[i-1] + ' ' + cand)
            if i != len(orig)-1:
                data[i][cand][feat_name + '.2ap'] = ngrMod.getBiProb(cand, orig[i+1], True)
                data[i][cand][feat_name + '.2af'] = ngrMod.getBiFreq(cand + ' ' +  orig[i+1])

def add(working, ngr_loc, feat_name):
    sys.stdout.write('loading ' + ngr_loc + '... ')
    sys.stdout.flush()
    ngrMod.init(ngr_loc)
    sys.stdout.write('done\nSearching ngrams... ')
    sys.stdout.flush()
    
    for sentId in os.listdir(working):
        sentFolder = working + sentId + '/'
        orig = [line[:-1] for line in open(sentFolder + 'orig', 'r', encoding='utf-8', errors='ignore')]
        
        data = pickle.load(open(sentFolder + 'combined.pickle', 'rb'))
        procSent(data, orig, feat_name)
        os.remove(sentFolder + 'combined.pickle')
        pickle.dump(data, open(sentFolder + 'combined.pickle', 'wb'), protocol=2)
    sys.stdout.write('done\n')
    sys.stdout.flush()
