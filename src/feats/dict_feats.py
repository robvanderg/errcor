#!/usr/bin/python3

import os
dicts = []
dictNames = ['aspell', 'scowl', 'hunspell']

for dictName in dictNames:
    dct = set()
    for line in open('data/' + dictName, 'r', encoding='utf-8', errors='ignore'):
        dct.add(line[:-1])
    dicts.append(dct)
    #names.append(dictPath[dictPath.rfind('/'):])

def find(word):
    word_data = []
    for itrDict in dicts:
        word_data.append(word in itrDict)
    return word_data

def getNames():
    return dictNames
