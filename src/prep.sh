# testdata
mkdir data
wget http://www.hlt.utdallas.edu/~chenli/normalization/2577_tweets 
mv 2577_tweets data/chenli
wget http://people.eng.unimelb.edu.au/tbaldwin/etc/lexnorm_v1.2.tgz
tar -zxvf lexnorm_v1.2.tgz
rm lexnorm_v1.2.tgz
mv data/corpus.v1.2.tweet data/lexnorm
rm data/README.txt

# w2v model
wget http://yuca.test.iminds.be:8900/fgodin/downloads/word2vec_twitter_model.tar.gz
tar -zxvf word2vec_twitter_model.tar.gz
mv word2vec_twitter_model/word2vec_twitter_model.bin data
rm -r word2vec_twitter_model.tar.gz word2vec_twitter_model

# dictionaries
aspell -dump master > data/aspell
wget https://noisy-text.github.io/files/scowl.american.70
mv scowl.american.70 data/scowl
wget http://downloads.sourceforge.net/wordlist/hunspell-en_US-2015.05.18.zip
unzip hunspell-en_US-2015.05.18.zip
mv en_US.dic data/hunspell
rm README_en_US.txt en_US.aff hunspell-en_US-2015.05.18.zip

#ngrams
wget http://clic.cimec.unitn.it/amac/twitter_ngram/en.1grams.gz
zgrep -a "^[a-zA-Z]" src/en.1grams.gz > data/tw.1
wget http://clic.cimec.unitn.it/amac/twitter_ngram/en.2grams.gz
zgrep -a "^[a-zA-Z]" src/en.2grams.gz > data/tw.2
python3.5 util/combine_ngrams.py
rm en.1grams.gz en.2grams.gz data/tw.1 data/tw.2

zgrep -a "^[a-zA-Z]" /net/corpora/5-gram/dvd1-6/data/1gms/vocab.gz > data/go.1
zcat /net/corpora/5-gram/dvd1-6/data/2gms/*.gz | grep "^[a-zA-Z]" > data/go.2


